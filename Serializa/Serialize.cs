﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;



namespace Serialize
{
    public class Serialize
    {
        public string ToCSVString(object o)
        {
            string retString = "";

            var fields = o.GetType().GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var f in fields)
            {

                var val = f.GetValue(o);

                var t = val.GetType();

                if (t.IsPrimitive && t != typeof(object))
                {

                    retString += val.ToString() + ";";
                }
                else if (t == typeof(String))
                {
                    retString += "\"" + val.ToString() + "\";";
                }
            }

            return retString;
        }


        public object FromCsvString<T>(string str)
        {

            var obj = Activator.CreateInstance(typeof(T));

            var strParams = str.Split(";");

            if(strParams.Last() == "\r\n")
            {
                strParams = strParams[..(strParams.Length-1)];
            }

            Type type = typeof(T);
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            int i = 0;

            foreach (var f in fields)
            {
                if (f.FieldType == typeof(int))
                {
                    f.SetValue(obj, Int32.Parse(strParams[i]));
                    i++;
                }   
                else if (f.FieldType == typeof(uint))
                {
                    f.SetValue(obj, UInt32.Parse(strParams[i]));
                    i++;
                }
                else if (f.FieldType == typeof(string))
                {
                    f.SetValue(obj, strParams[i]);
                    i++;
                }
                else
                {
                    throw (new NotImplementedException($"Тип поля {f.FieldType} не поддерживается десериализатором"));
                }
            }

            return obj;
        }
    }
}
