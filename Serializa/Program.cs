﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Serialize
{
    class Program
    {
        static void Main(string[] args)
        {
            Serialize serializer = new Serialize();
            TestClass testClass = TestClass.Get();
            string serString = "";
            string jsonString = "";

            Stopwatch timer = new Stopwatch();
            Stopwatch timer2 = new Stopwatch();
            Stopwatch timer3 = new Stopwatch();

            timer.Start();
            for (int i = 0; i < 100000; i++)
            {
                serString = serializer.ToCSVString(testClass);
            }
            timer.Stop();


            timer3.Start();
            for (int i = 0; i < 100000; i++)
            {
                jsonString = JsonConvert.SerializeObject(testClass);
            }
            timer3.Stop();

            timer2.Start(); 
            Console.WriteLine("Serialize to csv:");                       
            Console.WriteLine(serString);
            timer2.Stop();
            Console.WriteLine("");
            Console.WriteLine("Serialize to json:");
            Console.WriteLine(jsonString);
            Console.WriteLine("");
            Console.WriteLine("Console write time: " + timer2.ElapsedMilliseconds);
            Console.WriteLine("Serialize to csv time: " + timer.ElapsedMilliseconds);
            Console.WriteLine("Serialize to json time: " + timer3.ElapsedMilliseconds);
            Console.WriteLine("");

            timer.Restart();
            for (int i = 0; i < 100000; i++)
            {
                testClass = (TestClass)serializer.FromCsvString<TestClass>(serString);
            }
            timer.Stop();

            try
            {
                timer3.Restart();
                for (int i = 0; i < 100000; i++)
                {
                    testClass = JsonConvert.DeserializeObject<TestClass>(jsonString);
                }
                timer3.Stop();
            }
            catch (NotImplementedException ex)
            {
                Console.WriteLine($"Упс { ex.Message}");
            }
            
            Console.WriteLine("Deserialize from csv time:" + timer.ElapsedMilliseconds);
            Console.WriteLine("Deserialize from json time:" + timer3.ElapsedMilliseconds);
        }
    }
}
